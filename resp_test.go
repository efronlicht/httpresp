package httpresp_test

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/efronlicht/httpresp"
)

type notFoundError struct{}

func (n notFoundError) StatusCode() int {
	return http.StatusNotFound
}

func (n notFoundError) Error() string {
	return "not found"
}

type fooWriter struct{}

func (fooWriter) WriteTo(w io.Writer) (int64, error) {
	n, err := w.Write([]byte("foo"))
	return int64(n), err
}

var someJSON = struct{ Foo string }{"bar"}

func Test_WriteResp(t *testing.T) {
	for name, tt := range map[string]struct {
		input            interface{}
		wantCode         int
		wantBodyContains []string
	}{
		"just some JSON":                     {input: someJSON, wantCode: 200, wantBodyContains: []string{"foo", "bar"}},
		"normal unexposed error":             {input: errors.New("some error"), wantCode: 500, wantBodyContains: []string{"500", "internal server error"}},
		"io.WriterTo writes custom body":     {input: fooWriter{}, wantCode: 200, wantBodyContains: []string{"foo"}},
		"error with custom status (wrapped)": {input: fmt.Errorf("some error: %w", notFoundError{}), wantCode: http.StatusNotFound, wantBodyContains: []string{"404", "not found"}},
		"non-error with custom status":       {input: httpresp.WithCode{Body: someJSON, Code: http.StatusCreated}, wantCode: http.StatusCreated, wantBodyContains: []string{"foo", "bar"}},
		"concealed error":                    {input: httpresp.ConcealedError{Underlying: errors.New("some error"), Code: 404}, wantCode: 404, wantBodyContains: []string{"404", "not found"}},
		"exposed error (defaults to 500)":    {input: httpresp.ExposedError{Err: errors.New("exposed")}, wantCode: 500, wantBodyContains: []string{"exposed"}},
		"exposed error (custom code)":        {input: httpresp.ExposedError{Err: errors.New("exposed"), Code: 404}, wantCode: 404, wantBodyContains: []string{"exposed"}},
		"deadline exceeded (wrapped)":        {input: fmt.Errorf("some error: %w", context.DeadlineExceeded), wantCode: http.StatusRequestTimeout, wantBodyContains: []string{"408", "timeout"}},
	} {
		name, tt := name, tt
		t.Run(name, func(t *testing.T) {
			w := httptest.NewRecorder()
			_ = httpresp.WriteResp(w, tt.input)
			for _, shouldContain := range tt.wantBodyContains {
				if got := strings.ToLower(w.Body.String()); !strings.Contains(got, shouldContain) {
					t.Errorf("expected the body to contain %s, but got %s", tt.wantBodyContains, got)
				}
			}
			if w.Code != tt.wantCode {
				t.Errorf("expected code %d, but got %d", w.Code, tt.wantCode)
			}
		})
	}
}
