package httpresp

import (
	"encoding/json"
)

// these exactly correspond to the statuses in net/http, eg, http.StatusInternalServerError, and were ripped
// right from the source. see google_license.txt for more details.
const (
	StatusBadRequest                    StatusCodeError = 400 // RFC 7231, 6.5.1
	StatusUnauthorized                  StatusCodeError = 401 // RFC 7235, 3.1
	StatusPaymentRequired               StatusCodeError = 402 // RFC 7231, 6.5.2
	StatusForbidden                     StatusCodeError = 403 // RFC 7231, 6.5.3
	StatusNotFound                      StatusCodeError = 404 // RFC 7231, 6.5.4
	StatusMethodNotAllowed              StatusCodeError = 405 // RFC 7231, 6.5.5
	StatusNotAcceptable                 StatusCodeError = 406 // RFC 7231, 6.5.6
	StatusProxyAuthRequired             StatusCodeError = 407 // RFC 7235, 3.2
	StatusRequestTimeout                StatusCodeError = 408 // RFC 7231, 6.5.7
	StatusConflict                      StatusCodeError = 409 // RFC 7231, 6.5.8
	StatusGone                          StatusCodeError = 410 // RFC 7231, 6.5.9
	StatusLengthRequired                StatusCodeError = 411 // RFC 7231, 6.5.10
	StatusPreconditionFailed            StatusCodeError = 412 // RFC 7232, 4.2
	StatusRequestEntityTooLarge         StatusCodeError = 413 // RFC 7231, 6.5.11
	StatusRequestURITooLong             StatusCodeError = 414 // RFC 7231, 6.5.12
	StatusUnsupportedMediaType          StatusCodeError = 415 // RFC 7231, 6.5.13
	StatusRequestedRangeNotSatisfiable  StatusCodeError = 416 // RFC 7233, 4.4
	StatusExpectationFailed             StatusCodeError = 417 // RFC 7231, 6.5.14
	StatusTeapot                        StatusCodeError = 418 // RFC 7168, 2.3.3
	StatusMisdirectedRequest            StatusCodeError = 421 // RFC 7540, 9.1.2
	StatusUnprocessableEntity           StatusCodeError = 422 // RFC 4918, 11.2
	StatusLocked                        StatusCodeError = 423 // RFC 4918, 11.3
	StatusFailedDependency              StatusCodeError = 424 // RFC 4918, 11.4
	StatusTooEarly                      StatusCodeError = 425 // RFC 8470, 5.2.
	StatusUpgradeRequired               StatusCodeError = 426 // RFC 7231, 6.5.15
	StatusPreconditionRequired          StatusCodeError = 428 // RFC 6585, 3
	StatusTooManyRequests               StatusCodeError = 429 // RFC 6585, 4
	StatusRequestHeaderFieldsTooLarge   StatusCodeError = 431 // RFC 6585, 5
	StatusUnavailableForLegalReasons    StatusCodeError = 451 // RFC 7725, 3
	StatusInternalServerError           StatusCodeError = 500 // RFC 7231, 6.6.1
	StatusNotImplemented                StatusCodeError = 501 // RFC 7231, 6.6.2
	StatusBadGateway                    StatusCodeError = 502 // RFC 7231, 6.6.3
	StatusServiceUnavailable            StatusCodeError = 503 // RFC 7231, 6.6.4
	StatusGatewayTimeout                StatusCodeError = 504 // RFC 7231, 6.6.5
	StatusHTTPVersionNotSupported       StatusCodeError = 505 // RFC 7231, 6.6.6
	StatusVariantAlsoNegotiates         StatusCodeError = 506 // RFC 2295, 8.1
	StatusInsufficientStorage           StatusCodeError = 507 // RFC 4918, 11.5
	StatusLoopDetected                  StatusCodeError = 508 // RFC 5842, 7.2
	StatusNotExtended                   StatusCodeError = 510 // RFC 2774, 7
	StatusNetworkAuthenticationRequired StatusCodeError = 511 // RFC 6585, 6
)

var lookupString = map[StatusCodeError]string{
	400: `{"code": 400,"error": "Bad Request"}`,
	401: `{"code": 401,"error": "Unauthorized"}`,
	402: `{"code": 402,"error": "Payment Required"}`,
	403: `{"code": 403,"error": "Forbidden"}`,
	404: `{"code": 404,"error": "Not Found"}`,
	405: `{"code": 405,"error": "Method Not Allowed"}`,
	406: `{"code": 406,"error": "Not Acceptable"}`,
	407: `{"code": 407,"error": "Proxy Authentication Required"}`,
	408: `{"code": 408,"error": "Request Timeout"}`,
	409: `{"code": 409,"error": "Conflict"}`,
	410: `{"code": 410,"error": "Gone"}`,
	411: `{"code": 411,"error": "Length Required"}`,
	412: `{"code": 412,"error": "Precondition Failed"}`,
	413: `{"code": 413,"error": "Request Entity Too Large"}`,
	414: `{"code": 414,"error": "Request URI Too Long"}`,
	415: `{"code": 415,"error": "Unsupported Media Type"}`,
	416: `{"code": 416,"error": "Requested Range Not Satisfiable"}`,
	417: `{"code": 417,"error": "Expectation Failed"}`,
	418: `{"code": 418,"error": "I'm a teapot"}`,
	421: `{"code": 421,"error": "Misdirected Request"}`,
	422: `{"code": 422,"error": "Unprocessable Entity"}`,
	423: `{"code": 423,"error": "Locked"}`,
	424: `{"code": 424,"error": "Failed Dependency"}`,
	425: `{"code": 425,"error": "Too Early"}`,
	426: `{"code": 426,"error": "Upgrade Required"}`,
	428: `{"code": 428,"error": "Precondition Required"}`,
	429: `{"code": 429,"error": "Too Many Requests"}`,
	431: `{"code": 431,"error": "Request Header Fields Too Large"}`,
	451: `{"code": 451,"error": "Unavailable For Legal Reasons"}`,
	500: `{"code": 500,"error": "Internal Server Error"}`,
	501: `{"code": 501,"error": "Not Implemented"}`,
	502: `{"code": 502,"error": "Bad Gateway"}`,
	503: `{"code": 503,"error": "Service Unavailable"}`,
	504: `{"code": 504,"error": "Gateway Timeout"}`,
	505: `{"code": 505,"error": "HTTP Version Not Supported"}`,
	506: `{"code": 506,"error": "Variant Also Negotiates"}`,
	507: `{"code": 507,"error": "Insufficient Storage"}`,
	508: `{"code": 508,"error": "Loop Detected"}`,
	510: `{"code": 510,"error": "Not Extended"}`,
	511: `{"code": 511,"error": "Network Authentication Required"}`,
}

// lookup table for pre-written JSON responses for http status codes.
// identical to lookupString, but avoids an allocation during StatusCodeError.WriteTo.
var lookupBytes = map[StatusCodeError]json.RawMessage{
	400: []byte(`{"code": 400,"error": "Bad Request"}`),
	401: []byte(`{"code": 401,"error": "Unauthorized"}`),
	402: []byte(`{"code": 402,"error": "Payment Required"}`),
	403: []byte(`{"code": 403,"error": "Forbidden"}`),
	404: []byte(`{"code": 404,"error": "Not Found"}`),
	405: []byte(`{"code": 405,"error": "Method Not Allowed"}`),
	406: []byte(`{"code": 406,"error": "Not Acceptable"}`),
	407: []byte(`{"code": 407,"error": "Proxy Authentication Required"}`),
	408: []byte(`{"code": 408,"error": "Request Timeout"}`),
	409: []byte(`{"code": 409,"error": "Conflict"}`),
	410: []byte(`{"code": 410,"error": "Gone"}`),
	411: []byte(`{"code": 411,"error": "Length Required"}`),
	412: []byte(`{"code": 412,"error": "Precondition Failed"}`),
	413: []byte(`{"code": 413,"error": "Request Entity Too Large"}`),
	414: []byte(`{"code": 414,"error": "Request URI Too Long"}`),
	415: []byte(`{"code": 415,"error": "Unsupported Media Type"}`),
	416: []byte(`{"code": 416,"error": "Requested Range Not Satisfiable"}`),
	417: []byte(`{"code": 417,"error": "Expectation Failed"}`),
	418: []byte(`{"code": 418,"error": "I'm a teapot"}`),
	421: []byte(`{"code": 421,"error": "Misdirected Request"}`),
	422: []byte(`{"code": 422,"error": "Unprocessable Entity"}`),
	423: []byte(`{"code": 423,"error": "Locked"}`),
	424: []byte(`{"code": 424,"error": "Failed Dependency"}`),
	425: []byte(`{"code": 425,"error": "Too Early"}`),
	426: []byte(`{"code": 426,"error": "Upgrade Required"}`),
	428: []byte(`{"code": 428,"error": "Precondition Required"}`),
	429: []byte(`{"code": 429,"error": "Too Many Requests"}`),
	431: []byte(`{"code": 431,"error": "Request Header Fields Too Large"}`),
	451: []byte(`{"code": 451,"error": "Unavailable For Legal Reasons"}`),
	500: []byte(`{"code": 500,"error": "Internal Server Error"}`),
	501: []byte(`{"code": 501,"error": "Not Implemented"}`),
	502: []byte(`{"code": 502,"error": "Bad Gateway"}`),
	503: []byte(`{"code": 503,"error": "Service Unavailable"}`),
	504: []byte(`{"code": 504,"error": "Gateway Timeout"}`),
	505: []byte(`{"code": 505,"error": "HTTP Version Not Supported"}`),
	506: []byte(`{"code": 506,"error": "Variant Also Negotiates"}`),
	507: []byte(`{"code": 507,"error": "Insufficient Storage"}`),
	508: []byte(`{"code": 508,"error": "Loop Detected"}`),
	510: []byte(`{"code": 510,"error": "Not Extended"}`),
	511: []byte(`{"code": 511,"error": "Network Authentication Required"}`),
}
