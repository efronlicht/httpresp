package httpresp

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type (
	// ExposedError is an error that we expose to our clients.
	ExposedError struct {
		// Err to be exposed to the customer. Available via Unwrap().
		Err error `json:"error"`
		// Code is the http StatusCode. Optional. (Defaults to http.InternalServerError).
		Code int `json:"code"`
	}

	// ConcealedError is an error we don't expose to our clients.
	ConcealedError struct {
		// Underlying error which we log but don't expose.
		// NOT available via Unwrap.
		Underlying error `json:"error"`
		// Code is the http StatusCode. Optional.
		Code int `json:"code"`
	}
	// StatusCodeError writes simple error responses very efficiently.
	// Only the constants defined in this package are guaranteed.
	// See status_codes.go for details on the responses.
	StatusCodeError int
)

func (rwc WithCode) WriteTo(w io.Writer) (n int64, err error) {
	if wt, ok := rwc.Body.(io.WriterTo); ok {
		return wt.WriteTo(w)
	}
	b, err := json.Marshal(rwc.Body)
	if err != nil {
		return 0, err
	}
	m, err := w.Write(b)
	return int64(m), err
}

func (rwc WithCode) StatusCode() int {
	if rwc.Code == 0 {
		return http.StatusOK
	}
	return rwc.Code
}

// Unwrap the underlying ExposedError.Error
func (ee ExposedError) Unwrap() error { return ee.Err }

func (ee ExposedError) Error() string {
	return fmt.Sprintf("%d %s: %s", ee.Code, http.StatusText(ee.Code), ee.Err)
}

func (ee ExposedError) StatusCode() int {
	if ee.Code == 0 {
		return http.StatusInternalServerError
	}
	return ee.Code
}

func (ee ExposedError) WriteTo(w io.Writer) (int64, error) {
	m, err := fmt.Fprintf(w, `{"error": %q, "code": %d}`, ee.Err, ee.Code)
	return int64(m), err
}

func (ce ConcealedError) WriteTo(w io.Writer) (int64, error) {
	return StatusCodeError(ce.Code).WriteTo(w)
}
func (ce ConcealedError) StatusCode() int { return StatusCodeError(ce.Code).StatusCode() }
func (ce ConcealedError) Error() string {
	return fmt.Sprintf("%d %s: %s", ce.Code, http.StatusText(ce.Code), ce.Underlying)
}

func (s StatusCodeError) StatusCode() int { return int(s) }
func (s StatusCodeError) Error() string {
	return fmt.Sprintf("%d %s", s, http.StatusText(int(s)))
}

// WriteTo writes a JSON body to the underyling writer. See status_codes.go for what it will look like.
func (s StatusCodeError) WriteTo(w io.Writer) (n int64, err error) {
	if s < 300 || s >= 600 {
		return 0, fmt.Errorf("unknown status code %d", s)
	}
	m, err := w.Write(s.lookupBytes())
	return int64(m), err
}

// UnmarshalJSON by finding the "code" key.
func (s *StatusCodeError) UnmarshalJSON(data []byte) error {
	var tmp struct {
		Code int `json:"code"`
	}
	err := json.Unmarshal(data, &tmp)
	*s = StatusCodeError(tmp.Code)
	return err
}

// MarshalJSON by looking up the precomputed response. Just a wrapper around ResponseBodyBytes.
func (s *StatusCodeError) MarshalJSON() ([]byte, error) { return s.lookupBytes(), nil }

func (s StatusCodeError) lookupBytes() []byte {
	b := lookupBytes[s]
	if b == nil {
		b = []byte(fmt.Sprintf(`{"code": %d, "error": ""<unknown error %d>"`, s, s))
	}
	return b
}

func (s StatusCodeError) ResponseBody() string { return lookupString[s] }
