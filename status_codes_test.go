package httpresp

import (
	"net/http/httptest"
	"testing"
)

func Test_Status_Codes(t *testing.T) {
	for k, want := range lookupBytes {
		w := httptest.NewRecorder()
		WriteResp(w, k)
		if got := w.Body.String(); got != string(want) {
			t.Errorf("expected %s, but got %s", want, got)
		}
		if w.Code != int(k) {
			t.Errorf("expected %d, but got %d", w.Code, k)
		}
	}
}
