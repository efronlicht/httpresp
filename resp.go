// Package httpresp provides WriteResp, an all-purpose function for writing http responses,
// and a number of helper structs and interfaces to use it with.
// This is meant to be THE ONLY function that writes http responses.
// The rules are as follows:
// Response Header:
//	- StatusCoder => StatusCode()
// 	- context.DeadlineExceeded => http.StatusRequestTimeout
//  - error => http.StatusInternalServerError
//  - default => http.StatusOK
// Response Body:
//	- io.WriterTo => WriteTo
//	- error => ConcealedError{resp, code}.WriteTo
//  - default => json.Marshal(resp)
package httpresp

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"net/http"
)

var (
	_ CustomResponder = WithCode{}
	_ json.Marshaler  = (*StatusCodeError)(nil)
)

type (
	CustomStatusCoder interface{ StatusCode() int }
	// CustomResponder provides a custom status code and response body.
	CustomResponder interface {
		CustomStatusCoder
		io.WriterTo
	}

	// WithCode is a response that provides a custom status Code and writes Body.
	// It's not meant for errors. Use the types in errors.go (ExposedError, StatusCodeError, ConcealedError) instead.
	WithCode struct {
		// Code is the http StatusCode. Optional (defaults to http.StatusOK).
		Code int
		// Body to be written. If it implements io.WriterTo, use that. Otherwise, marshal as JSON.
		Body interface{}
	}
)

// WriteResp writes a status code header and response body to w.
// This is meant to be THE ONLY function that writes http responses.
// Response Header (Status Code): chosen according to the following rules.
//  - StatusCoder => StatusCode()
//	- error  w/ errors.As StatusCoder => StatusCode()
// 	- error w/ errors.Is context.DeadlineExceeded => http.StatusRequestTimeout
//  - error (other) => http.StatusInternalServerError
//  - default => http.StatusOK
// Response Body:
//	- io.WriterTo => WriteTo
//	- error => StatusCodeError(code).WriteTo
//  - default => json.Marshal(resp)
// Bodies are written with io.WriterTo
func WriteResp(w http.ResponseWriter, resp interface{}) (err error) {
	if resp == nil {
		panic("nil resp")
	}
	code := StatusCode(resp)
	w.WriteHeader(code)
	return writeBody(w, code, resp)
}

// Response Body:
//	- io.WriterTo => WriteTo
//	- error => StatusCodeError(code).WriteTo
//  - default => json.Marshal(resp)
// Bodies are written with io.WriterTo
func writeBody(w io.Writer, code int, resp interface{}) (err error) {
	switch resp := resp.(type) {
	case io.WriterTo:
		_, err = resp.WriteTo(w)
		return err
	case error:
		_, err = StatusCodeError(code).WriteTo(w)
		return err
	default:
		return json.NewEncoder(w).Encode(resp)
	}
}

// StatusCode chooses a http response status code according to the following rulset.
//	- StatusCoder => StatusCode()
//	- error  w/ errors.As StatusCoder => StatusCode()
// 	- error w/ errors.Is context.DeadlineExceeded => http.StatusRequestTimeout
//  - error (other) => http.StatusInternalServerError
//  - default => http.StatusOK
func StatusCode(resp interface{}) (code int) {
	switch v := resp.(type) {
	case CustomStatusCoder:
		return v.StatusCode()
	case error:
		if errors.Is(v, context.DeadlineExceeded) {
			return http.StatusRequestTimeout
		}
		if coder := CustomStatusCoder(nil); errors.As(v, &coder) {
			return coder.StatusCode()
		}

		return http.StatusInternalServerError
	default:
		return http.StatusOK
	}
}
